public class RegularPolygon {
    private int n = 3;
    private double side = 1.0;
    private double x = 0.;
    private double y = 0.;

    // Constructors
    RegularPolygon() {
    }

    RegularPolygon(int sides, double length) {
        this.n = sides;
        this.side = length;
    }

    RegularPolygon(int sides, double length, double centerX, double centerY) {
        this.n = sides;
        this.side = length;
        this.x = centerX;
        this.y = centerY;
    }


    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getPerimeter() {
        return this.n * this.side;
    }

    public double getArea() {
        return (this.n * Math.pow(this.side, 2)) / (4 * Math.tan(Math.PI / this.n));
    }
}
