public class PolygonTest {
    public static void main(String[] args) {
        RegularPolygon defaultConstructor = new RegularPolygon();
        RegularPolygon sideOnlyConstructor = new RegularPolygon(6, 4);
        RegularPolygon allMemberConstructor = new RegularPolygon(10, 4, 5.6, 7.8);

        System.out.println("Default Constructor Polygon:");
        displayPerimeterArea(defaultConstructor);

        System.out.println("Side Only Constructor Polygon:");
        displayPerimeterArea(sideOnlyConstructor);


        System.out.println("All Member Constructor Polygon:");
        displayPerimeterArea(allMemberConstructor);
    }

    private static void displayPerimeterArea(RegularPolygon polygon) {
        System.out.printf("Perimeter: %.3f\n", polygon.getPerimeter());
        System.out.printf("Area: %.3f\n", polygon.getArea());

        System.out.println();
    }
}
